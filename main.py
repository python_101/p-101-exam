import random

choice_list = ["Rock", "Paper", "Scissors"]

print("Welcome to the Rock Paper Scissors game.")

computer_choice = random.choice(choice_list)

while True:
    prompt = input("Choose between Rock/Paper/Scissors (or 'quit' to exit): ").capitalize()

    if prompt == "Quit":
        print("Thanks for playing!")
        break

    if prompt not in choice_list:
        print("Invalid choice. Please choose between Rock, Paper or Scissors.")
        continue

    computer_choice = random.choice(choice_list)

    if prompt == computer_choice:
        print(f"Computer chose {computer_choice} and it's a draw!")
    elif (prompt == "Rock" and computer_choice == "Scissors") or \
            (prompt == "Paper" and computer_choice == "Rock") or \
            (prompt == "Scissors" and computer_choice == "Paper"):
        print(f"Computer chose {computer_choice} and you win!")
    else:
        print(f"Computer chose {computer_choice} and you lose!")
